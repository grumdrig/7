#!/usr/bin/env python

"""Version control operations independent of choice of VCS. Also, some
shortcuts of various sorts.
Usage: 7 [OPTS] COMMAND [ARGS]
where OPTS COMMAND ARGS are as expected by the VCS in use in the current
directory, with the addition or override of the following commands

help                 this info (call the VCS directly for that 'help')
situation|sitch|sit  comprehensive VCS state of directory (test if
					 no changes vs remote repo, or say why not)
						 -a argument lists situation of all project dirs
remote               what is the remote repo?
vcs|scm              what VCS controls this directory?
in                   checkin using ARGS as comment and push to remote (commit -am and push)
df                   custom diff command which might work better for me
stat                 alias for 'status', even for git
"""

# Info on converting svn to hg:
# http://hedonismbot.wordpress.com/2008/10/16/hg-fast-export-convert-mercurial-repositories-to-git-repositories/
# PYTHONPATH=/opt/subversion/lib/svn-python/ hg convert --authors authors PROJECT PROJECT-hg


import sys, os, subprocess


HG = 'hg'
GIT = 'git'
SVN = 'svn'

_vcs = {}
def vcs(root, loud=True):
	global _vcs
	if root not in _vcs:
		reroot = ["-R", root] if root != "." else []
		hg = False #(os.path.isdir(os.path.join(root,".hg")) or
					#command(['hg'] + reroot + ['-q','stat']).returncode == 0) and HG
		git = os.path.join(root,'.git')
		git = (os.path.isdir(git) or
					 command(['git','--git-dir',git,'rev-parse']).returncode==0) and GIT
		svn = os.path.isdir(os.path.join(root,".svn")) and SVN

		count = len(filter(None,[hg,git,svn]))
		if count != 1:
			if loud:
				print >>sys.stderr, 'Uncertain version control system'
			sys.exit(count or -1)

		_vcs[root] = hg or git or svn

	return _vcs[root]


def parenvcs(root):
	return '(' + vcs(root) + ')'


def command(args, visible=False):
	pipe = None if visible else subprocess.PIPE
	p = subprocess.Popen(args, stdout=pipe, stderr=pipe)
	p.wait()
	return p


def seven(root, *params):
	return nine(root, params, visible=True)


def silent7(root, *params):
	return nine(root, params, visible=False)


def nine(root, params, visible):
	args = [vcs(root)]
	if root != '.':
		if vcs(root) == HG:
			args.extend(['-R', root])
		elif vcs(root) == GIT:
			args.extend(['--git-dir', os.path.join(root,'.git'), '--work-tree',root])
	args.extend(params)
	if root != '.' and vcs(root) == SVN:
		args.append(root)
	if visible: print '#', ' '.join(map(repr, args))
	return command(args, visible)


def main():
	args = sys.argv[1:]

	root = args.pop(0) if (args and os.path.isdir(args[0])) else '.'
	cmd = args.pop(0) if args else None

	if cmd == 'in':
		if not args:
			print >>sys.stderr, 'No commit message specified'
			sys.exit(1)
		seven(root, 'commit', '-am', ' '.join(args))
		if vcs(root) != SVN:
			seven(root, 'push')
	elif cmd == 'df':
		#os.system("%s stat | egrep '^M'" % (vcs(root),));
		os.system("%s status | egrep '^M' | cut -d' ' -f7 | xargs -n 1 %s diff | less" % (vcs(root),vcs(root)));
	elif cmd in ('situation','sitch','sit'):
		dirs = args or ['.']
		if dirs == ['-a']:
			dirs = (subdirs(os.path.expanduser("~/src/")) +
							[os.path.expanduser(os.path.join("~/", x))
							 for x in "Emacs Sites bin home".split()])
		for root in dirs:
			situation(root)
	elif cmd == 'remote':
		print remote((args or [root])[0])
	elif cmd in ('vcs', 'scm'):
		print vcs(root)
	elif (cmd == 'help') and not args:
		print __doc__.strip()
		sys.exit(0)
	else:
		if cmd == 'stat':
			cmd = 'status'
		seven(root, *([cmd] + args))


def remote(root):
	if vcs(root) == HG:
		p = silent7(root, 'paths', 'default')
		if p.returncode == 0:
			return p.stdout.read().strip()
	elif vcs(root) == GIT:
		p = silent7(root, 'remote', '-v')
		if p.returncode == 0:
			for line in p.stdout.readlines():
				remote,path,action = line.split()
				if action == '(push)':
					return path
	elif vcs(root) == SVN:
		p = silent7(root, 'info')
		info = dict([line.strip().split(": ",1)
								 for line in p.stdout.readlines() if line.strip()])
		return info['URL']


def changes(root):
	if vcs(root) == HG:
		p = silent7(root, 'status')
	elif vcs(root) == GIT:
		p = silent7(root, 'status', '-s')
	elif vcs(root) == SVN:
		p = silent7(root, 'status')
	return p.stdout.read()


def outgoing(root):
	if vcs(root) == HG:
		p = silent7(root, 'outgoing')
		return p.stdout.read().find('no changes found') < 0
	elif vcs(root) == GIT:
		p = silent7(root, 'push', '-n')
		out = p.stderr.read()
		return out.find('Everything up-to-date') < 0
	else:
		return False


'''
# HOWTO CREATE AN HG REMOTE on grumdrig.com
ssh grumdrig.com mkdir hg/src/$NAME
ssh grumdrig.com hg init $_
hg init
echo "[paths]" >> .hg/hgrc
hg clone ssh://grumdrig.com:hg/src/$NAME
echo "default = $_" >> .hg/hgrc
hg push
'''

def situation(root):
	try:
		vcs(root, loud=False)
	except SystemExit as (code,):
		if code > 0:
			raise
		print "UNTRACKED", root
		return

	if not remote(root):
		print 'NO-REMOTE', root, parenvcs(root)
		return  # enuf said

	if changes(root):
		print 'CHANGES', root, parenvcs(root)
		return  # that'll do, pig

	if outgoing(root):
		print 'OUTGOING', root, parenvcs(root), remote(root)
		return  # just for symmetry


def subdirs(root):
	return [os.path.join(root,f)
			for f in os.listdir(root)
			if os.path.isdir(os.path.join(root, f)) and f[:1] != '.']


if __name__ == "__main__":
	main()
